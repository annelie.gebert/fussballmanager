package de.awacademy.fussballmanager;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ClubController {
    List<Club> clubList = new ArrayList<>();
    List<Player> playerList = new ArrayList<>(); // zur besseren Übersicht, macht es an manchen Stellen einfacher

    public void createDummies() {
        clubList.add(new Club("HSV",12));
        clubList.add(new Club("Werder", 22));
        clubList.add(new Club("EMTV", 19));

        playerList.add(new Player("Annelie", "1", "HSV"));
        clubList.get(0).getPlayerList().add(playerList.get(0));

        playerList.add(new Player("Thomas", "3", "HSV"));
        clubList.get(0).getPlayerList().add(playerList.get(1));

        playerList.add(new Player("Elise", "4", "EMTV"));
        clubList.get(2).getPlayerList().add(playerList.get(2));

        playerList.add(new Player("Henry", "2", "Werder"));
        clubList.get(1).getPlayerList().add(playerList.get(3));

    }
    @GetMapping("/")
    public String showIndex(Model model) {
        /* zum testen
        if(clubList.isEmpty()){
            createDummies();
        }*/

        List<Club> sortedClubList = clubList.stream()
                .sorted(Comparator.comparingInt(Club::getPoints).reversed())
                .collect(Collectors.toList());
        model.addAttribute("clubList", sortedClubList);
        return "index";
    }

    @GetMapping("/createClub") // wenn dieser Link aufgerufen wird mache folgendes
    public String showCreateClub(Model model) {
        Club club = new Club("", 0); // dieser Club wird an die html-Seite geschickt, also ein Platzhalten
        model.addAttribute("club", club); // hiermit wird der Club an createClub geschickt

        return "createClub"; // zeige die Seite createClub.html
    }

    @PostMapping("/formCreateClub")
    public String formCreateClub(@ModelAttribute Club club, Model model) {
        if (club.getName().isEmpty()) {
            return "failMissingValues";
        }

        club.setId();
        club.setPlayerList();

        clubList.add(club);
        model.addAttribute("club", club);
        return "createClubSuccess";
    }

    @GetMapping("/clubDetails")
    public String showClubDetails(Model model, @RequestParam int clubId) {

        model.addAttribute("club", findClubById(clubId));
        return "clubDetails";
    }

    @GetMapping("/playerDetails")
    public String showPlayerDetails(Model model, @RequestParam int playerId) {
        Player player = findPlayer(playerId);
        Club club = findClub(player.getClub());

        model.addAttribute("player", player);
        model.addAttribute("club", club);
        return "playerDetails";
    }

    @PostMapping("/formCreatePlayer")
    public String formCreatePlayer(@ModelAttribute ("player") Player player, Model model) {
        player.setId();

        if(player.getClub().isEmpty() || player.getName().isEmpty()){
            return "failMissingValues";
        }

        Club club = findClub(player.getClub());

        club.getPlayerList().add(player); // füge den Player zur Playerlist des Clubs hinzu
        playerList.add(player); // füge den Player zur gesamten Playerlist hinzu

        model.addAttribute("club",club);

        return "createPlayerSuccess";
    }

    @GetMapping("/createPlayer") // wenn dieser Link aufgerufen wird mache folgendes
    public String showCreatePlayer(Model model, @RequestParam int clubId) {

        Player player = new Player("", "", ""); // dieser Player wird an die html-Seite geschickt, also ein Platzhalten

        model.addAttribute("player", player); // hiermit wird der Club an createClub geschickt
        model.addAttribute("clubList", clubList);
        model.addAttribute("clubId", clubId);

        return "createPlayer"; // zeige die Seite createClub.html
    }

    @GetMapping("/updatePlayer")
    public String showUpdatePlayer(Model model, @RequestParam int playerId) {
        Player player = findPlayer(playerId);

        model.addAttribute("player", player); // der Spieler, der geändert werden soll
        model.addAttribute("clubList", clubList); // für das Dropdown-Menü

        return "updatePlayer";
    }

    @PostMapping("/formUpdatePlayer")
    public String formUpdatePlayer(Model model, @ModelAttribute ("player") Player player, @RequestParam int playerId) {
        if (player.getName().isEmpty()) {
            return "failMissingValues";
        }
        Player oldPlayer = findPlayer(playerId); // alter Player
        Club oldClub = findClub(oldPlayer.getClub()); // alter Club des players

        player.resetId(oldPlayer.getPlayerId()); // hier wird nochmal die id gesetzt für den geänderten Player
        Club newClub = findClub(player.getClub()); // Club des geänderten Players

        if (player.getClub().equals(oldPlayer.getClub())){ // wenn der Club nicht geändert wurde
            updatePlayerInClubPlayerList(player);
        } else { // wenn der Club geändert wurde
            // lösche player in der playerList des alten Vereins
            oldClub.getPlayerList().remove(oldPlayer);

            // füge den player in die playerList des neuen Vereins
            newClub.getPlayerList().add(player);
        }

        // jetzt muss der geänderte player noch in der playerList geändert werden
        boolean flag = true; // zeigt an, ob der player gefunden wurde
        int i=0;
        do {
            if (playerList.get(i).getPlayerId() == playerId) {
                flag = false;
                playerList.set(i, player); // Änderung des Players in der allgemeinen Liste
            }
            i++;
        } while (flag);

        model.addAttribute("player", player);

        return "updatePlayerSuccess";
    }

    @PostMapping("/formDeletePlayer")
    public String formDeletePlayer(Model model, @RequestParam int playerId) {
        Player player = findPlayer(playerId);
        Club club = findClub(player.getClub());

        // player in der playerList des club löschen
        club.getPlayerList().remove(player);
        // wir lassen es noch in der playerList, falls der Kunde versehentlich gelöscht hat und den player
        // wiederherstellen möchte, können wir das machen

        model.addAttribute("player", player);

        return "deletePlayerSuccess";
    }
// ###########################################################################################
    public void updatePlayerInClubPlayerList(Player player) {
        // Ändert den Player in der playerList des Vereins

        Club club = findClub(player.getClub()); // finde den Club in clubList
        boolean flag = true; // zeigt an, ob der player gefunden wurde
        int i=0;
        do {
            if (club.getPlayerList().get(i).getPlayerId() == player.getPlayerId()) { // wenn der player gefunden wurde
                flag = false;
                club.getPlayerList().set(i, player); // Änderung in der playerList des Clubs
            }
            i++;
        } while (flag);
    }

    public Club findClub(String clubName) {
        Club club = new Club();
        for (Club c : clubList) {
            if (c.getName().equals(clubName)) {
                club = c;
            }
        }
        return club;
    }

    public Club findClubById(int id) {
        Club club = new Club();
        for (Club c : clubList) {
            if (c.getId() == id) {
                club = c;
            }
        }
        return club;
    }

    public Player findPlayer(int playerId) {
        Player player = new Player();
        for (Player p : playerList) {
            if (p.getPlayerId() == playerId) {
                player = p;
            }
        }
        return player;
    }


}
