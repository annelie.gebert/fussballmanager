package de.awacademy.fussballmanager;

public class Player {
    private static int playerCounter;
    private String name;
    private String club;
    private String jerseyNumber;
    private int id;

    public Player() {}
    public Player(String name, String jerseyNumber, String club) {
        this.club = club;
        this.name = name;
        this.jerseyNumber = jerseyNumber;
        this.setId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(String jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public int getPlayerId() {
        return id;
    }

    public void setId() {
        if(!this.name.equals("")){
            playerCounter++;
            this.id = playerCounter;
        }
    }

    public void resetId(int id) {
        this.id = id;
    }

}
