package de.awacademy.fussballmanager;

import java.util.ArrayList;

public class Club {
    private String name;
    private static int idcounter;
    private int points;
    private ArrayList<Player> playerList;
    private int id;

    public Club(){}
    public Club(String name, int points) {
        this.name = name;
        this.setId();
        this.points = points;
        this.playerList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public static void setIdcounter(int idcounter) {
        Club.idcounter = idcounter;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setId() {
        if(!this.getName().equals("")) {
            idcounter++;
            this.id = idcounter;
        }
    }

    public void setPlayerList() {
        this.playerList = new ArrayList<>();
    }

    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

}
