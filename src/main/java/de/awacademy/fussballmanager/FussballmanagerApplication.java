package de.awacademy.fussballmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FussballmanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FussballmanagerApplication.class, args);
	}

}
